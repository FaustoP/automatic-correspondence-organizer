﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

/* The purpose of this class is to run on start-up of the program, and properly map out the directories
 * whilst keeping this information in memory, stored in a dictionary within an object
 * the dictionary will contain the key - cubs number, and the value - the object containing the file path information
 */

namespace FileScannerOrganizer
{
    internal class Mapper
    {
        public const string Rootfolder = @"\\itgcubs\itgcubs$\prodcubsdata\FF_Data\CubsImages\NYLit\Debtor Files\CUBS\";
        public static Dictionary<string, CubsCaseObject> Database = new Dictionary<string, CubsCaseObject>();

        static Mapper()
        {
            var output = @"c:\output.txt";
            var stopWatch = new Stopwatch();
            stopWatch.Start();

            Console.WriteLine("Mapping out the directories beginning at the root folder: {0} \n", Rootfolder);
            // Input all the sub-folders within the rootfolder in this array, so we can analyze them one at a time.
            var directories = Directory.GetDirectories(Rootfolder);
            Console.WriteLine("{0} sub-directories found within root folder", directories.Length);

            // For each sub directory found in our root directory..
            foreach (var dir in directories)
            {
                //Now we're mapping all the cubs folders within each sub directory.
                var subdirectories = Directory.GetDirectories(dir);
                var count = 1;
                
                foreach(var subdir in subdirectories)
                {
                    var cubsNumber = new DirectoryInfo(subdir).Name;
                    var currentCase = new CubsCaseObject(subdir);
                    var completion = (count*100)/subdirectories.Length;
                    Console.Write("\rMapping {0} Progress to Completion: {1}%", dir.Remove(0, 79), completion);
                    try
                    {
                        //Simply try to add the key and Cubs Object to the dictionary.
                        Database.Add(cubsNumber, currentCase);
                    }
                    catch (Exception)
                    {
                        //The exception here will handle duplicate entries into the Database. It will happen, theres no way around it unless we delete the duplicate cubs folders in their old locations.
                        CubsCaseObject potentialDuplicateCaseObject;

                        //What this does is create a reference to the duplicate object.
                        Database.TryGetValue(cubsNumber, out potentialDuplicateCaseObject);
                        //Just to be on the safe side, as long as that object is not null, which it shouldn't be, do:
                        if (potentialDuplicateCaseObject != null)
                        {
                            //Set the original duplicate object's duplicate flag to true;
                            potentialDuplicateCaseObject.DuplicateExists = true;
                            // Lets hold onto the cubsNumber.
                            var cubsNumberPlaceholder = potentialDuplicateCaseObject.CubsNumber;
                            // Delete the original dictionary entry as we will have to re-enter it using the appended case type.
                            Database.Remove(cubsNumber);
                            // Below is the original Case, the key has just been changed to ######-CaseType.
                            Database.Add(cubsNumberPlaceholder + "-" + potentialDuplicateCaseObject.CaseType,
                                potentialDuplicateCaseObject);
                            // This is the second instance of the key, what triggered our exception in the first place. We will append the case type to it as well.
                            Database.Add(cubsNumber + "-" + currentCase.CaseType, currentCase);
                        }
                    }
                    count++;
                }
            }
            //The following is for logging purposes - It will log the output to output.txt in the C:\ folder. I don't think I added a clause for the creation of the file if it doesn't already exist. So beware of that.
            File.WriteAllText(output, string.Empty);
            using (var sw = File.AppendText(output))
            {
                // I wanted to see the output sorted, Dictionarys are inherently unordered, and so I convert it to a SortedDictionary.
                var sortedDatabase = new SortedDictionary<string, CubsCaseObject>(Database);
                foreach (var cubsCase in sortedDatabase)
                {
                    //Write to a new line every case in our sorted dictionary.
                    sw.WriteLine("Cubs Key: {0} || Cubs Number: {1} || Case Type {2}", cubsCase.Key,
                        cubsCase.Value.CubsNumber, cubsCase.Value.CaseType);
                }
            }
            //This just stops the watch, as I was running it for diagnostics. The last Console statement here just prints the elapsed time.
            stopWatch.Stop();
            Console.WriteLine("\nTOTAL TIME REQUIRED FOR MAPPING \n");
            var ts = stopWatch.Elapsed;
            var elapsedTime = string.Format("{0:00}:{1:00}:{2:00}.{3:00} \n",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds/10);
            Console.WriteLine(elapsedTime);
        }
    }
}