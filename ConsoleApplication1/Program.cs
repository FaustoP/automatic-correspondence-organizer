﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Security.Permissions;
using System.Threading;
using System.Windows.Forms;
using Microsoft.WindowsAPICodePack.Dialogs;
using Microsoft.WindowsAPICodePack.Shell.PropertySystem;

namespace FileScannerOrganizer
{
    internal class Program
    {
        public const string Constfilepath = @"C:\scans\";
        public const string ConstfilePathUnique = @"C:\scan\";
        public bool Spencer = false;

        private static void Main(string[] args)
        {
            Map();
            Watch();
        }

        private static void Map()
        {
            new Mapper();
        }

        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        private static void Watch()
        {

            try
            {
                string setFilePathTo = string.Empty;
                setFilePathTo = DefaultScanFolder(Constfilepath) ? Constfilepath : ConstfilePathUnique;
                Console.WriteLine("FileScannerOrganizer.exe is set to watch {0}", setFilePathTo);
                //Create the Watcher object, set it's path to our default scan path.
                var watcher = new FileSystemWatcher
                {
                    
                    Path = setFilePathTo,
                    NotifyFilter =
                        NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName |
                        NotifyFilters.DirectoryName,
                    Filter = "*.pdf"
                };

                //Set up for events involving LastAccessed, Last modified, file name changes or changes to the directory name.
                //Watch for pdf files, only.

                //Event handlers
                watcher.Created += OnCreated;
                watcher.Deleted += OnChanged;
                watcher.Renamed += OnRenamed;

                watcher.EnableRaisingEvents = true;

                Console.WriteLine("Press \'q\' to quit");
                while (Console.Read() != 'q')
                {
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("The following is the exception: {0}", ex);
            }
        }

        private static bool DefaultScanFolder(string defaultPath)
        {
            if (!Directory.Exists(defaultPath))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private static void OnCreated(object sender, FileSystemEventArgs e)
        {
            // seperated creation from change because I'd like to add an event trigger for when the file is created.
            Console.WriteLine("File: {0} {1}", e.FullPath, e.ChangeType);
            var newPdf = new PdfObject(e.Name, e.FullPath);
            var newTask = new Tasker(newPdf);
            RoutineLogger log = new RoutineLogger();
           
            if (newTask.ContainsDuplicates)
            {
                ProperPathPrompt(newTask);
                newTask.Proceed = true;
            }
            if (!newTask.Proceed)
            {
                if (newTask.MissingXmg)
                {
                    log.ErrorCannotMoveFile(newTask);
                }
                return;
            }
            try
            {
                //This is how we're going to actually move the file, the simplest part of this whole program.
                newTask.MoveFile(newTask.CurrentTask);
                log.LogToFile(newTask);

                //Notification of Said movement.
                var t = new Thread(() => GenerateNotifier(newTask, e));
                t.Start();
                

                if (newTask.RenameToSummonsAndComplaint)
                {
                    log.RenamedFile(newTask);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(
                    "Exception when attempting to move file from location to destination - OnCreated Event Method: \n\r {0}",
                    ex.ToString());
            }

            //Console.WriteLine("DEBUG: CurrentTask File Name: {0} ; Destination Folder: {1};",
            //  newTask.CurrentTask.FileName, newTask.Destination);
        }

        private static void GenerateNotifier(Tasker newTask, FileSystemEventArgs e)
        {
            System.Media.SystemSounds.Beep.Play();
            
            var notifier = new NotifyIcon()
            {
                Icon = new Icon("FileWatcherIcon.ico"),
                BalloonTipTitle = string.Format("File {0}", e.Name),
                BalloonTipText =
                    string.Format(
                        newTask.Destination.Remove(0,79)),
                Visible = true,

            };
            notifier.ShowBalloonTip(4000);
            notifier.BalloonTipClicked +=
                new EventHandler((deliverer, o) =>
                {
                    notifier.Dispose();
                    System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo()
                    {
                        FileName = newTask.Destination,
                        UseShellExecute = true,
                        Verb = "open"
                    });
                });
            
            Application.Run();
            notifier.Visible = false;
        }

        private static void ProperPathPrompt(Tasker newTask)
        {
            var pathOne = newTask.DuplicateCases[0].FilePath.Remove(0, 79);
            var pathTwo = newTask.DuplicateCases[1].FilePath.Remove(0, 79);
            var destinationSet = false;
            do
            {
                //Play a sound when a duplicate case dialog appears.
                System.Media.SystemSounds.Hand.Play();
                //Lets create the dialog.
                var taskDialog = new TaskDialog
                {
                    Caption = "Duplicate cases prompt",
                    InstructionText =
                        string.Format("There is more than one location for the CUBS case {0}",
                            newTask.CurrentTask.CubsNumber),
                    Text =
                        "Please press the button below that correctly indicates where you would like to place the scanned file.",
                    Icon = TaskDialogStandardIcon.Warning,
                    FooterIcon = TaskDialogStandardIcon.Warning
                };
                //The options
                var commandLink1 = new TaskDialogCommandLink("Case1", pathOne);
                var commandLink2 = new TaskDialogCommandLink("Case2", pathTwo);
                commandLink1.Click += caseOneCommand_Click;
                commandLink2.Click += caseTwoCommand_Click;
                taskDialog.Controls.Add(commandLink1);
                taskDialog.Controls.Add(commandLink2);
                var tdr = taskDialog.Show();
                //Show the dialog

                switch (tdr)
                {
                        //First case
                    case TaskDialogResult.Ok:
                    {
                        newTask.Destination = newTask.DuplicateCases[0].FilePath;
                        MessageBox.Show(string.Format(@"You've chosen to place the file in {0}", pathOne));
                        destinationSet = true;
                        break;
                    }
                        // Second case
                    case TaskDialogResult.No:
                    {
                        newTask.Destination = newTask.DuplicateCases[1].FilePath;
                        MessageBox.Show(string.Format(@"You've chosen to place the file in {0}", pathTwo));
                        destinationSet = true;
                        break;
                    }
                        //This case should never occur.
                    default:
                    {
                        MessageBox.Show("No path chosen, default");
                        break;
                    }
                }
            } while (!destinationSet);
            newTask.Proceed = true;
            Console.WriteLine("\n\r The file has been moved to {0}", newTask.Destination);
        }

        private static void caseTwoCommand_Click(object sender, EventArgs e)
        {
            var tcl2 = (TaskDialogCommandLink) sender;
            ((TaskDialog) tcl2.HostingDialog).Close(TaskDialogResult.No);
        }

        private static void caseOneCommand_Click(object sender, EventArgs e)
        {
            var tcl1 = (TaskDialogCommandLink) sender;
            ((TaskDialog) tcl1.HostingDialog).Close((TaskDialogResult.Ok));
        }

        private static void OnChanged(object sender, FileSystemEventArgs e)
        {
            // Information on what is done when the file is changed/created or deleted.
            Console.WriteLine("File: {0} {1}", e.FullPath, e.ChangeType);
        }

        private static void OnRenamed(object sender, RenamedEventArgs e)
        {
            Console.WriteLine("File: {0} renamed to {1}", e.OldName, e.Name);
        }
    }
}