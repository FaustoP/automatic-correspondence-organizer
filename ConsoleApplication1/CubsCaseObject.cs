﻿using System.IO;

namespace FileScannerOrganizer
{
    internal class CubsCaseObject
    {
        public CubsCaseObject(string filePath)
        {
            FilePath = filePath;
            //Just the name of the final directory in the path, we just want the cubs number anyway.
            CubsNumber = new DirectoryInfo(filePath).Name;
            var caseType = _getCaseType(filePath);
            // Below is purely for removing the "NYCI" portion Case Types as they're listed in the Filepath and inserting it into the CaseType as just the type of case it is. Ex: NYCIHRA2 --> HRA2.
            CaseType = caseType.Remove(0, 4);
            // If a duplicate key exists, this will be changed from the false to true. This is used by the Tasker/Mapper.
            DuplicateExists = false;
        }

        public string FilePath { get; set; }
        public string CubsNumber { get; set; }
        public string CaseType { get; set; }
        public bool DuplicateExists { get; set; }

        private string _getCaseType(string filePath)
        {
            // Delimit by \, then return the caseType as it will always be array element length -2.
            var delimitedFilePath = filePath.Split('\\');
            return delimitedFilePath[delimitedFilePath.Length - 2];
        }
    }
}