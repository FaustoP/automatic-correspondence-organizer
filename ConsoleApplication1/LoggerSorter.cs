﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileScannerOrganizer
{
    internal class LoggerSorter
    {
        private const string RootFolder = @"C:\FileScannerLog\";

        private void CreateRoot()
        {
            if (Directory.Exists(RootFolder))
            {
                Console.WriteLine("Root folder {0} already exists. \n\r", RootFolder);
            }
            else
            {
                Directory.CreateDirectory(RootFolder);
                Console.WriteLine("Logging directory does not exist, creating it now at {0}", RootFolder);
            }
        }

        private void CreateDateFolders()
        {
            DateTime current = DateTime.Now;
            var fileYear = Path.Combine(RootFolder, current.Year.ToString());
            var fileYearMonth = Path.Combine(fileYear, current.Month.ToString());
            var fileYearMonthDay = Path.Combine(fileYearMonth, current.Day.ToString());
            try
            {
                if (!Directory.Exists(fileYearMonthDay))
                {
                    Directory.CreateDirectory(fileYearMonthDay);
                    Console.WriteLine("Created filepath {0} as it did not exist previously.", fileYearMonthDay);
                }
            }
            catch (Exception dateLoggerDirectoryException)
            {
                Console.WriteLine("Exception with the LoggerSorter archives creation, {0}", dateLoggerDirectoryException);
            }

        }
    }
}
