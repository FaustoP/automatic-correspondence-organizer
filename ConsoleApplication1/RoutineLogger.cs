﻿/* This Class is simply for logging all the file copys that occur. 
 * Its stored in C:\FileScannerLog\FileScanLog.txt
 */

using System;
using System.Globalization;
using System.IO;

namespace FileScannerOrganizer
{
    internal class RoutineLogger
    {
        private const string RootPath = @"C:\FileScannerLog\";
        private const string LogFilePath = @"C:\FileScannerLog\FileScanLog.txt";

        private static void CheckLogExistence()
        {
            if (!Directory.Exists(GetCurrentDateFilePath()))
            {
                Directory.CreateDirectory(GetCurrentDateFilePath());
                Console.WriteLine("FileScanner Log File for today's date did not exist, so it was created at {0} \n\r",
                    RootPath);
            }
            var fileScannerLogFullPath = Path.Combine(GetCurrentDateFilePath(), GetCurrentDateFileName());
            var logFileExists = File.Exists(fileScannerLogFullPath);
            if (logFileExists)
            {
                Console.WriteLine("FileScan log file verified to exist at: {0} \r\n", LogFilePath);
            }
            else
            {
                DateTime time = DateTime.Now;
                var format = "M d h:mm yyyy";
                TextWriter tw = new StreamWriter(fileScannerLogFullPath, true);
                tw.WriteLine(
                    "This is the log file for File scanning, this file was created on the following date: {0} \r\n",
                    time.ToString(format));
                Console.WriteLine("Log File did not previously exist, created at the following date and time: {0} \r\n",
                    time.ToString((format)));
                tw.Close();
            }
        }

        public void LogToFile(Tasker currentTask)
        {
            CheckLogExistence();
            var time = DateTime.Now;
            var format = "[M/d/yyyy] hh:mm";
            try
            {
                var sw = new StreamWriter(Path.Combine(GetCurrentDateFilePath(), GetCurrentDateFileName()), true);
                sw.WriteLine(@"[{0}] - PDF File '{1}' moved from '{2}' to Destination folder '{3}'.",
                    time.ToString("f", CultureInfo.CreateSpecificCulture("en-US")), currentTask.CurrentTask.FileName,
                    currentTask.CurrentTask.FilePath, currentTask.Destination);
                sw.Close();
            }
            catch (Exception erorrLogging)
            {
                Console.WriteLine("Error Logging to file, exception: \r\n {0}", erorrLogging);
            }
        }

        public void ErrorCannotMoveFile(Tasker currentTask)
        {
            CheckLogExistence();
            var time = DateTime.Now;
            var sw = new StreamWriter(LogFilePath, true);
            sw.WriteLine(
                @"[{0}] - ************* WARNING: DID NOT MOVE '{1}' FROM '{2}' BECAUSE NO XMG FOLDER EXISTS. *************",
                time.ToString("f", CultureInfo.CreateSpecificCulture("en-US")), currentTask.CurrentTask.FileName,
                currentTask.CurrentTask.FilePath);
            sw.Close();
        }

        public void RenamedFile(Tasker currentTask)
        {
            CheckLogExistence();
            var time = DateTime.Now;
            var format = "[M/d/yyyy] hh:mm";
            try
            {
                var sw = new StreamWriter(Path.Combine(GetCurrentDateFilePath(), GetCurrentDateFileName()), true);
                sw.WriteLine(@"[{0}] - File {1} Renamed to {2}",
                    time.ToString("f", CultureInfo.CreateSpecificCulture("en-US")), currentTask.CurrentTask.FileName,
                    currentTask.RenamedFilePath);
                sw.Close();
            }
            catch (Exception renamingFileError)
            {
                Console.WriteLine("Erorr when trying to log a file rename event, {0}", renamingFileError);
            }
        }

        public static string GetCurrentDateFileName()
        {
            var currentDate = DateTime.Now;
            var fileNameFormat = "M-d-yyyy";
            var fileNameShouldBe = currentDate.ToString(fileNameFormat);
            var fileNameFull = fileNameShouldBe + " - FileScannerLog.txt";
            return fileNameFull;
        }

        private static string GetCurrentDateFilePath()
        {
            var currentDate = DateTime.Now;
            //Creating the string for appending to the filepath.
            var appendDateFilePath =
                Path.Combine((Path.Combine(currentDate.Year.ToString(), currentDate.Month.ToString())),
                    currentDate.Day.ToString());
            //Creating the fullpath by conc. the root folder and the filepath.
            var fullPath = Path.Combine(RootPath, appendDateFilePath);
            return fullPath;
        }
    }
}