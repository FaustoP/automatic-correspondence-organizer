﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace FileScannerOrganizer
{
    class ConCurrentMapping
    {
        public const string Rootfolder = @"\\itgcubs\itgcubs$\prodcubsdata\FF_Data\CubsImages\NYLit\Debtor Files\CUBS\";
        public static ConcurrentDictionary<string, CubsCaseObject> ConcurrentDatabase = new ConcurrentDictionary<string, CubsCaseObject>(); 
        static ConCurrentMapping()
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            var directories = Directory.GetDirectories(Rootfolder);

            foreach(var dir in directories)
            {
                var subdirectories = Directory.GetDirectories(dir);
                int count = 1;
                //Console.WriteLine("Working on Subdirectory {0}", dir);

                Parallel.ForEach(subdirectories, subdir =>
                {
                    var completion = (count*100)/subdirectories.Length;
                    Console.Write("\r Subdir {0}, Completion: {1}", subdir, completion);
                   // Console.WriteLine("\r Mapping {0}, Progress thus far: {1}", subdir, completion);
                    var cubsNumber = new DirectoryInfo(subdir).Name;
                    var currentCase = new CubsCaseObject(subdir);
                    count++;
                    
                    if (!ConcurrentDatabase.TryAdd(cubsNumber, currentCase))
                    {
                        CubsCaseObject duplicateCaseOriginal;
                        ConcurrentDatabase.TryGetValue(cubsNumber, out duplicateCaseOriginal);
                        if (duplicateCaseOriginal != null)
                        {
                            duplicateCaseOriginal.DuplicateExists = true;
                            var cubsNumberPlaceHolder = duplicateCaseOriginal.CubsNumber;
                            CubsCaseObject nullHolder;
                            ConcurrentDatabase.TryRemove(cubsNumber, out nullHolder);
                            var cubsStringDuplicateCase = cubsNumberPlaceHolder + "-" + duplicateCaseOriginal.CaseType;
                            ConcurrentDatabase.TryAdd(cubsStringDuplicateCase, duplicateCaseOriginal);
                            ConcurrentDatabase.TryAdd(cubsNumber + "-" + currentCase.CaseType, currentCase);
                            Console.WriteLine("Duplicate case detected involving {0}", cubsStringDuplicateCase);
                        }
                        else
                        {
                            ConcurrentDatabase.TryAdd(cubsNumber, currentCase);
                            Console.WriteLine("Successfully added {0}", cubsNumber);
                        }
                    }
                    return;
                });
            }
            Console.WriteLine("Mapping completely complete. Writing to file for debug...");
            var output = @"c:\output.txt";
            File.WriteAllText(output, string.Empty);
            using (var sw = File.AppendText(output))
            {
                // I wanted to see the output sorted, Dictionarys are inherently unordered, and so I convert it to a SortedDictionary.
                var sortedDatabase = new SortedDictionary<string, CubsCaseObject>(ConcurrentDatabase);
                foreach (var cubsCase in sortedDatabase)
                {
                    //Write to a new line every case in our sorted dictionary.
                    sw.WriteLine("Cubs Key: {0} || Cubs Number: {1} || Case Type {2}", cubsCase.Key,
                        cubsCase.Value.CubsNumber, cubsCase.Value.CaseType);
                }
            }
            stopWatch.Stop();
            Console.WriteLine("\n TOTAL TIME REQUIRED FOR MAPPING \n");
            var ts = stopWatch.Elapsed;
            var elapsedTime = string.Format("{0:00}:{1:00}:{2:00}.{3:00} \n",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds / 10);
            Console.WriteLine(elapsedTime);
        }
    }
}
