﻿/* The objective of the Tasker class is to grab a PDFObject's cubs number
 * loop through open windows, find a window with a matching cub number, retrieve that window's
 * file path, and move a copy of the PDF file to that directory.
 */

using System;
using System.IO;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;

namespace FileScannerOrganizer
{
    internal class Tasker
    {
        public Tasker(PdfObject currentPdf)
        {
            Proceed = true;
            DuplicateCases = new List<CubsCaseObject>();
            CurrentTask = currentPdf;
            RenameToSummonsAndComplaint = currentPdf.IsSummonsAndComplaint;
            if (currentPdf.CubsNumber != 0)
            {
                CubsCaseObject temporaryObjectPlaceHolder;
                if (Mapper.Database.TryGetValue(currentPdf.CubsNumber.ToString(), out temporaryObjectPlaceHolder))
                {
                    Console.WriteLine("Mapper Filepath found, object filepath set.");
                    CubsCase = temporaryObjectPlaceHolder;
                    Destination = CubsCase.FilePath;
                }
                else
                {
                    foreach (var key in Mapper.Database)
                    {
                        if (key.Value.CubsNumber.Contains(currentPdf.CubsNumber.ToString()))
                        {
                            ContainsDuplicates = true;
                            DuplicateCases.Add(key.Value);
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("File {0} is not appropriately named, will not be moving this file.",
                    currentPdf.FileName);
                Proceed = false;
            }
            if (Destination == null)
            {
                Console.WriteLine("No XMG folder exists for this file... We cannot move it.");
                Proceed = false;
                MissingXmg = true;
            }
        }

        public PdfObject CurrentTask { get; set; }
        public string Destination { get; set; }
        public CubsCaseObject CubsCase { get; set; }
        public bool ContainsDuplicates { get; set; }
        public List<CubsCaseObject> DuplicateCases { get; set; }
        public bool Proceed { get; set; }
        public bool MissingXmg { get; set; }
        public bool RenameToSummonsAndComplaint { get; set; }
        public string RenamedFilePath { get; set; }
        public string DestinationFullPath { get; set; }
        
        
        public void MoveFile(PdfObject currentTask)
        {
            //This waits while the printer is actually done producing the image/pdf.
            FileInfo file = new FileInfo(currentTask.FilePath);
            long lastlength;
            do
            {
                lastlength = file.Length;
                Console.WriteLine("\n\rFile still being generated, Tasker blocking until file done being generated.");
                Thread.Sleep(1000);
                file.Refresh();
            } while (file.Length > lastlength);

            var end = Path.Combine(Destination, currentTask.FileName);
            try
            {
                System.IO.File.Copy(currentTask.FilePath, end, true);
                Console.WriteLine("File moved to {0}\n\r", Destination);
                if (RenameToSummonsAndComplaint)
                {
                    RenameFileToSummonsAndComplaint(end);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("There was an exception when trying to move the file \n\r: {0}", e.ToString());
            }
        }

        private void RenameFileToSummonsAndComplaint(string file)
        {
            int count = 1;
            // get the Files extension
            string extension = Path.GetExtension(file);
            // get the path of the file
            string path = Path.GetDirectoryName(file);
            // Create the endpath of what we want the file to be in the end.
            string endPath = Path.Combine(path, @"S&C" + extension);

            while (File.Exists(endPath)) //While that path exists.
            {
              string tempFileName = string.Format("{0}({1})", "S&C", count++);
              endPath = Path.Combine(path, tempFileName + extension);
            }
            File.Move(file, endPath);
            RenamedFilePath = endPath;
            Console.WriteLine("File has been renamed to:  {0}", endPath);

        }


    }
}
