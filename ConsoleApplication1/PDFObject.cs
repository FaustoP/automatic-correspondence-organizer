﻿/* The purpose of this class is simply to create the template of an object that stores the meta
 * information of the PDF file that we are going to be moving to another location. This includes
 * the creation date, file name, and the full file path of where the file is currently located.
 */

using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace FileScannerOrganizer
{
    internal class PdfObject
    {
        public PdfObject(string fileName, string filePath)
            : this(fileName, filePath, DateTime.Now)
        {
        }

        public PdfObject(string fileName, string filePath, DateTime creationDate)
        {
            FileName = fileName;
            FilePath = filePath;
            FileCreationTime = creationDate;
            IsSummonsAndComplaint = GetIsSummonsAndComplaint(fileName);
            /* Now in order to pull the cubs number, we will attempt to retrieve it from the fileName
             * which must be delimited in the following manner for example: 123456_ECB1
             * This allows the following code to check for cubs number.
             */
            if (fileName.Contains("_"))
            {
                var cubsNumberResult = GetPotentialCubsNumberFromFileName(fileName);

                /* If the value of cubsNumberResult = 0, it means our program failed to parse a CubsNumber from
                 * the file name of the PDF. Therefore we will not bother setting the CubsNumber.
                 */
                if (cubsNumberResult != 0)
                {
                    CubsNumber = cubsNumberResult;
                }
            }
        }

        public string FileName { get; set; }
        public string FilePath { get; set; }
        public DateTime FileCreationTime { get; set; }
        public int CubsNumber { get; set; }
        public string RootFolder { get; set; }
        public string PathDestination { get; set; }
        public bool IsSummonsAndComplaint { get; set; }
        /*
        public int GetCubsNumberFromFileName(string fileName)
        {
            var cubsNumberArray = fileName.Split('_');
            try
            {
                int result;
                int.TryParse(cubsNumberArray[0], out result);
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(
                    "There was an exception when attempting to parse the string Format of the CubsNumber from the FileName. Exception: {0}",
                    ex);
                return 0;
            }
        }
        */

        //Unlike my previous method, this one will get the CubsNumber from any location in the name.
        private int GetPotentialCubsNumberFromFileName(string fileName)
        {
            var cubsNumberArray = fileName.Split('_');
            try
            {
                foreach (var index in cubsNumberArray)
                {
                    Regex cubsRegex = new Regex(@"^[0-9]{6}$");
                    if (cubsRegex.Match(index).Success)
                    {
                        int result;
                        int.TryParse(index, out result);
                        return result;
                    }
                }
            }
            catch (Exception)
            {
                Console.WriteLine("There was an exception when attempting to parse the string format of CubsNumber from the FileName.");
                return 0;
            }
            return 0;
        }

        private bool GetIsSummonsAndComplaint(string fileName)
        {
            var nameArray = fileName.Split('_');
            try
            {
                if (nameArray.Any(index => index.ToUpper() == "SC"))
                {
                    return true;
                }
            }
            catch (Exception)
            {
                {
                    Console.WriteLine("There was an exception when checking if the account was a Summons and complaint");
                    return false;
                }
            }
            return false;
        }
    }
}